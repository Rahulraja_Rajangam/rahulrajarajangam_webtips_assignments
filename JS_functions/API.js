import { script } from "../script.js";
// import { time } from "./time.js";
// import { Daily } from "./Object.js";
// import { Display } from "./display.js";
export let Daily={}
let Dummy_object={}
var requestOptions = {
  method: 'GET',
  redirect: 'follow'
};
let interval_variable
function data_fetching(){fetch("https://soliton.glitch.me/all-timezone-cities", requestOptions)
  .then(response => response.json())
  .then(result => {
    Object.assign(Dummy_object,result)
    for (const key in Dummy_object) {
      Dummy_object[key].nextFiveHrs= [
        "6°C",
        "7°C",
        "11°C",
        "2°C"
      ]
      Daily[Dummy_object[key].cityName]=Dummy_object[key]
    }
    console.log(Daily)
  })
  .then(()=>{script()})
  .catch(error => console.log('error', error));
}
data_fetching()
clearInterval(interval_variable)
interval_variable=setInterval(data_fetching,6000000)


//let data_city=[{"cityName":"Nome","dateAndTime":"8/20/2021, 9:24:07 PM","timeZone":"America/Nome","temperature":"8°C","humidity":"83%","precipitation":"16%"},{"cityName":"NewYork","dateAndTime":"8/21/2021, 1:24:07 AM","timeZone":"America/New_york","temperature":"3°C","humidity":"93%","precipitation":"6%"},{"cityName":"Jamaica","dateAndTime":"8/21/2021, 12:24:07 AM","timeZone":"America/Jamaica","temperature":"29°C","humidity":"39%","precipitation":"58%"},{"cityName":"LosAngeles","dateAndTime":"8/20/2021, 10:24:07 PM","timeZone":"America/Los_Angeles","temperature":"19°C","humidity":"60%","precipitation":"38%"},{"cityName":"Winnipeg","dateAndTime":"8/21/2021, 12:24:07 AM","timeZone":"America/Winnipeg","temperature":"17°C","humidity":"64%","precipitation":"34%"},{"cityName":"Juba","dateAndTime":"8/21/2021, 8:24:07 AM","timeZone":"Africa/Juba","temperature":"10°C","humidity":"79%","precipitation":"20%"},{"cityName":"Maseru","dateAndTime":"8/21/2021, 7:24:07 AM","timeZone":"Africa/Maseru","temperature":"36°C","humidity":"25%","precipitation":"72%"},{"cityName":"London","dateAndTime":"8/21/2021, 6:24:07 AM","timeZone":"Europe/London","temperature":"-1°C","humidity":"100%","precipitation":"0%"},{"cityName":"Vienna","dateAndTime":"8/21/2021, 7:24:07 AM","timeZone":"Europe/Vienna","temperature":"25°C","humidity":"47%","precipitation":"50%"},{"cityName":"Moscow","dateAndTime":"8/21/2021, 8:24:07 AM","timeZone":"Europe/Moscow","temperature":"9°C","humidity":"81%","precipitation":"18%"},{"cityName":"Dublin","dateAndTime":"8/21/2021, 6:24:07 AM","timeZone":"Europe/Dublin","temperature":"22°C","humidity":"54%","precipitation":"44%"},{"cityName":"Karachi","dateAndTime":"8/21/2021, 10:24:07 AM","timeZone":"Asia/Karachi","temperature":"22°C","humidity":"54%","precipitation":"44%"},{"cityName":"Kolkata","dateAndTime":"8/21/2021, 10:54:07 AM","timeZone":"Asia/Kolkata","temperature":"32°C","humidity":"33%","precipitation":"64%"},{"cityName":"Yangon","dateAndTime":"8/21/2021, 11:54:07 AM","timeZone":"Asia/Yangon","temperature":"28°C","humidity":"41%","precipitation":"56%"},{"cityName":"BangKok","dateAndTime":"8/21/2021, 12:24:07 PM","timeZone":"Asia/BangKok","temperature":"30°C","humidity":"37%","precipitation":"60%"},{"cityName":"Seoul","dateAndTime":"8/21/2021, 2:24:07 PM","timeZone":"Asia/Seoul","temperature":"22°C","humidity":"54%","precipitation":"44%"},{"cityName":"Anadyr","dateAndTime":"8/21/2021, 5:24:07 PM","timeZone":"Asia/Anadyr","temperature":"-22°C","humidity":"100%","precipitation":"0%"},{"cityName":"BrokenHill","dateAndTime":"8/21/2021, 2:54:07 PM","timeZone":"Australia/Broken_Hill","temperature":"17°C","humidity":"64%","precipitation":"34%"},{"cityName":"Perth","dateAndTime":"8/21/2021, 1:24:07 PM","timeZone":"Australia/Perth","temperature":"9°C","humidity":"81%","precipitation":"18%"},{"cityName":"Auckland","dateAndTime":"8/21/2021, 5:24:07 PM","timeZone":"Pacific/Auckland","temperature":"17°C","humidity":"64%","precipitation":"34%"},{"cityName":"Vostok","dateAndTime":"8/21/2021, 11:24:07 AM","timeZone":"Antarctica/Vostok","temperature":"-61°C","humidity":"100%","precipitation":"0%"},{"cityName":"Troll","dateAndTime":"8/21/2021, 7:24:07 AM","timeZone":"Antarctica/Troll","temperature":"-65°C","humidity":"100%","precipitation":"0%"}]
// let Daily={}
// for (index=0;index<data_city.length;index++){
// Object.assign(Daily,data_city)}
// console.log(Daily)

//   let prop = "jamaica";
//   let city
//   for(element in Daily){
//   if(Daily[element].cityName.toLowerCase()==prop)
//     city=Daily[element]
//   }
//   console.log(city)
//   return city;
