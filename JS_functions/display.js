export function Display(weatherType, sunny, rainy, cold) {
  let slide = document.querySelector(".SFrame .slide");
  let end=this.value
  let climate_key = eval(weatherType);
  let i = 0;
  slide.innerHTML = "";
  end = end > 10 ? 10 : end;
  end = end < 3 ? 3 : end;
  if (weatherType == "sunny") {
    climate_key.sort((a, b) => {
      return b.temperature.replace("°C", "") - a.temperature.replace("°C", "");
    });
  } else if (weatherType == "cold") {
    climate_key.sort((a, b) => {
      return (
        b.precipitation.replace("%", "") - a.precipitation.replace("%", "")
      );
    });
  } else {
    climate_key.sort((a, b) => {
      return b.humidity.replace("%", "") - a.humidity.replace("%", "");
    });
  }
  for (let index = 0; index < end; index++) {
    if (end > climate_key.length) {
      end = climate_key.length;
    }
    let climate_obj = eval(climate_key[i]);
    let citytime = new Date(climate_obj.dateAndTime);
    let cityam_pm = "am";
    let cityhour = citytime.getHours();
    let citydate =
      citytime.getDay() +
      "-" +
      (citytime.getMonth() + 1) +
      "-" +
      citytime.getFullYear();
    if (citytime.getHours() > 12) {
      cityhour = citytime.getHours() - 12;
      cityam_pm = "pm";
    }
    cityhour = ("0" + cityhour).slice(-2);
    let weather_icon = weatherType == "cold" ? "snowflake" : weatherType;
    slide.innerHTML += `<div class="box" id="first${i}"style="background-image: url('assests/Icons for cities/${
      climate_obj.cityName
    }.svg');background-position-x: 80px;
      background-position-y: 120px;">
    <div class="place">
      <span class="pla">${climate_obj.cityName}</span>
      <span class="temp">
        <img src="assests/Weather Icons/${weather_icon}Icon.svg" id="${weather_icon}" alt="${weather_icon}"/> ${
      climate_obj.temperature
    }</span>
    </div>
    <div class="time">${
      cityhour + ":" + (("0"+citytime.getMinutes()).slice(-2)) + " " + cityam_pm
    }</div>
    <div class="day">${citydate}</div>
    <div class="prediction1">
      <img src="assests/Weather Icons/humidityIcon.svg" alt="humidity"/>
      <div class="data">${climate_obj.humidity}</div>
    </div>
      <div class="prediction2">
      <img src="assests/Weather Icons/precipitationIcon.svg" alt="precipitation"/>
      <div class="data">${climate_obj.precipitation}</div>
      </div>
    </div>`;
    i++;
  }
}
