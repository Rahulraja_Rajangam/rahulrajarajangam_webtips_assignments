let  dt, h=0, m=0, s=0, d=0, mo=0, y=0, functionvariable, am_pm, month;
export function time(ob, i) {
  if (i == 0) {
    clearTimeout(functionvariable);
    dt = new Date(ob);
    h = dt.getHours();
    m = dt.getMinutes();
    s = dt.getSeconds();
    d = dt.getDate();
    mo = dt.getMonth();
    y = dt.getFullYear();
    month = [
      "JAN",
      "FEB",
      "MAR",
      "APR",
      "MAY",
      "JUN",
      "JUL",
      "AUG",
      "SEP",
      "OCT",
      "NOV",
      "DEC",
    ];
  }
  document.querySelector("span.bold").innerHTML = `${
    ("0" + (h>12?h-12:h)).slice(-2) + ":" + ("0" + m).slice(-2)
  }<span class="small">:${("0" + s).slice(-2)}</span>`;
  document.getElementById("date").innerText =
    ("0" + d).slice(-2) + "-" + month[mo] + "-" + y;
  s++;
  if (s == 60) {
    m++;
    s = 0;
  }
  if (m == 60) {
    h++;
    s = 0;
    m = 0;
  }
  if (h == 24) {
    d++;
    m = 0;
    h = 0;
    s = 0;
  }
  if (
    mo == 0 ||
    mo == 2 ||
    mo == 4 ||
    mo == 6 ||
    mo == 7 ||
    mo == 9 ||
    mo == 11
  ) {
    if (d > 31) {
      mo++;
      d = 1;
    }
  } else {
    if (mo == 1) {
      if (d > 28) {
        mo++;
        d = 1;
      }
    } else {
      if (d > 30) {
        mo++;
        d = 1;
      }
    }
  }
  if (mo > 12) {
    y++;
    mo = 0;
  }
  am_pm = h >= 12 ? "PM" : "AM";
  if (am_pm == "PM") {
    document.querySelector(".Datetime img").src =
      "assests/General Images & Icons/pmState.svg";
  }
  hourly_timeline();
  functionvariable = setTimeout(time, 1000);
}
function hourly_timeline() {
  let hour = document.querySelector(".Hourly").children;
  let timeline = h;
  let hour1, ham_hpm;
  timeline += 1;
  for (let i = 2; i < 11; i += 2) {
    ham_hpm = timeline > 11 && timeline <= 24 ? "pm" : "am";
    hour1 = timeline > 12 ? timeline - 12 : timeline;
    hour[i].children[0].innerText = hour1 + " " + ham_hpm;
    timeline++;
    timeline = timeline > 24 ? 1 : timeline;
  }
}