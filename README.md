## Weather Report Website

# Section - 1 (Navigation->Nav)

1)Flex and Grid is used to create the layout.
2)Css selectors to select HTML elements for styling.
3)Media query to change styles based on window resolution.
4)Overriding styles using CSS specificity rules.

---

# Sedction - 2 (FFrame,SFrame)

1. Flex and grid is used for layout.
   2)Spinner to customize the number of cities displayed under the top cities should be present with a minimum and maximum value defined as 3 and 10 respectively.
   3)CSS transforms on hovering over each city and use Z index to make it appear over the overall section background.

---

# Section - 3 (GFrame)

1. Animations for the corresponding card.
2. Flex and grid is used for layout.