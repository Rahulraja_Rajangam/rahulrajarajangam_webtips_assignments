import { time } from "./JS_functions/time.js";
import { Daily } from "./JS_functions/API.js";
import { Display } from "./JS_functions/display.js";
export function script()
{
console.log(Daily)
let Temp = document.querySelector(".Temp");
let tempchild = Temp.children;
let data_key = Object.keys(Daily);
let weatherType = "sunny";
let city_array = [];
let array_key;
let sunny = [];
let cold = [];
let rainy = [];
let width_,city_inheritance;
var initial_sort_array=[];
let slidevalue = eval(300);
let city_h = 0,city_m = 0,city_am_pm;
let Sort_function_conti = 0,Sort_function_temp = 0;
let CI_box_time, loop_index,conti_time_function, box;
let x, index, Select_value, key,temp_sort_flag=1,conti_sort_flag=1;
let sun = document.querySelector(".FFrame .TFFrame img#sun");
let rain = document.querySelector(".FFrame .TFFrame img#rain");
let snow = document.querySelector(".FFrame .TFFrame img#snow");
let display = document.querySelector(".Displaytop #quantity");
let continent_sort_img=document.querySelector(".GFrame .heading .cn ")
let temperature_sort_img=document.querySelector(".GFrame .heading #temperature")
for (array_key in Daily) {city_array.push(Daily[array_key]);}
sunny = city_array.filter(temperature_sunny);
cold = city_array.filter(temperature_cold);
rainy = city_array.filter(temperature_rainy);

document.querySelector(".SFrame button.bbtn").addEventListener("click", ()=>{document.querySelector(".SFrame .slide").scrollLeft += slidevalue;});
document.querySelector(".SFrame button").addEventListener("click", ()=>{document.querySelector(".SFrame .slide").scrollLeft -= slidevalue;});
document.getElementById("sel").addEventListener("change", select_city);
document.querySelector(".GFrame .heading .cn ").addEventListener("click",()=>{Sort_function_conti=(Sort_function_conti==0)?1:0;Sort_function()})
document.querySelector(".GFrame .heading #temperature").addEventListener("click",()=>{Sort_function_temp=(Sort_function_temp==0)?1:0;Sort_function()})


sun.onclick = function () {
  weatherType = "sunny";
  Display.call(display, weatherType, sunny, rainy, cold);
  clearInterval(CI_box_time);
  CI_box_time = setInterval(function(){box_time(".SFrame .slide")}, 60000);
  document.querySelector(".Frame .FFrame .icon #sun").style.borderBottom =
    "2px solid var(--blue)";
  document.querySelector(".Frame .FFrame .icon #rain").style.borderBottom =
    "0px";
  document.querySelector(".Frame .FFrame .icon #snow").style.borderBottom =
    "0px";
};
rain.onclick = function () {
  weatherType = "rainy";
  Display.call(display, weatherType, sunny, rainy, cold);
  clearInterval(CI_box_time);
  CI_box_time = setInterval(function(){box_time(".SFrame .slide")}, 60000);
  document.querySelector(".Frame .FFrame .icon #sun").style.borderBottom =
    "0px";
  document.querySelector(".Frame .FFrame .icon #rain").style.borderBottom =
    "2px solid var(--blue)";
  document.querySelector(".Frame .FFrame .icon #snow").style.borderBottom =
    "0px";
};
snow.onclick = function () {
  weatherType = "cold";
  Display.call(display, weatherType, sunny, rainy, cold);
  clearInterval(CI_box_time);
  CI_box_time = setInterval(function(){box_time(".SFrame .slide")}, 60000);
  document.querySelector(".Frame .FFrame .icon #sun").style.borderBottom =
    "0px";
  document.querySelector(".Frame .FFrame .icon #snow").style.borderBottom =
    "2px solid var(--blue)";
  document.querySelector(".Frame .FFrame .icon #rain").style.borderBottom =
    "0px";
};
display.onchange = function () {
  Display.call(this, weatherType, sunny, rainy, cold);
  clearInterval(CI_box_time);
  CI_box_time = setInterval(box_time, 60000);
};

class City_details{
  constructor(city)
  {
    this.cityName=city.cityName
    this.dateAndTime=city.dateAndTime
    this.timeZone=city.timeZone
    this.temperature=city.temperature
    this.humidity=city.humidity
    this.precipitation=city.precipitation
    this.nextFiveHrs=city.nextFiveHrs
  }


temp_change(flag){
  // ---------------Temp grid data-----------------------
  let city_key = Object.keys(this);
  let i = 3;
  if (flag == 0) {
    for (let index = 0; index < tempchild.length; index++) {
      if (index == 2) {
        tempchild[index].children[1].innerHTML =
          (this[city_key[3]].replace("°C", "") * 1.8 + 32).toFixed(0) + " F";
      } else {
        tempchild[index].children[1].innerHTML = this[city_key[i++]];
      }
    }
  } else {
    for (let index = 0; index < tempchild.length; index++) {
      tempchild[index].children[1].innerHTML = "NIL";
    }
    alert("Invalid City");
  }
}
}
class city_child_details extends City_details{
  constructor(city){
    if(city!=undefined)
    super(city)
    else{
      let key={}
      key.cityName="Null"
    key.dateAndTime="Null"
    key.timeZone="Null"
    key.temperature="Null"
    key.humidity="Null"
    key.precipitation="Null"
    key.nextFiveHrs="Null"
    super(key)
    }
  }

hourly_change(flag){
  let hour = document.querySelector(".Hourly").children;
  if (flag==1) {
    for (index = 0; index < 11; index += 2){
      hour[index].children[3].innerText = "Nil";
      (hour[index].children[2]).children[0].src ="assests/General Images & Icons/warning.svg" 
    }
    return;
  }
  // ---------------Hourly img and data----------------
  let hourtem = [0];
  let s
  hourtem[0] = this.temperature;
  hourtem.push(...this.nextFiveHrs);
  for ( s in hourtem) {
    hourtem[s] = hourtem[s].replace("°C", "");
  }
  hourtem.push(this.temperature.replace("°C", ""))
  let j = 0;
  for (let index = 0; index < 11; index += 2) {
    let image = hour[index].children[2];
    let weather =
      hourtem[j] < 18
        ? "rainyIconBlack"
        : hourtem[j] >= 18 && hourtem[j] <= 22
        ? "windyIcon"
        : hourtem[j] >= 23 && hourtem[j] <= 29
        ? "cloudyIcon"
        : "sunnyIconBlack";
    image.querySelector("img").src = `assests/Weather Icons/${weather}.svg`;
    if (hourtem[j] != undefined)
      {hour[index].children[3].innerText = hourtem[j] + "°C";
      console.log(hourtem[j])}
    // else {
    //   hour[index].children[3].innerText =
    //     Math.floor(Math.random() * (40 - 30)) + 30 + "°C";
    // }
    j++;
  }
  // ----------------------------------------------------

}
}

// city_child_details.prototype=Object.create(Current_city_details.prototype)
// Null_city.prototype=Object.create(Current_city_details.prototype)
//initial sort
initial_sort_array.push(...city_array.sort((a, b) => {
  let fn=a.cityName
  let sn=b.cityName
  if(fn>sn){
    return 1
  }
  if(fn<sn){
    return -1
  }
return 0;}))

//scroll button
clearInterval(width_)
width_=setInterval(width_button_visible,100)
// ===================================================First Container Function========================================
// Selection
async function select_city() {
  Select_value = document.getElementById("sel").value;
  x = city("cityName", Select_value);
  console.log(Select_value)
  city_inheritance=new city_child_details(x)
  let flag = 0;
  let city_detail_result={}
  if (x == undefined) {
    x = "Nil";
    flag = 1;
    city_inheritance.hourly_change(flag)
    city_inheritance.temp_change(flag);
    document.querySelector(".Nav .logo img").src = `assests/General Images & Icons/warning.svg`;
    document.querySelector(".Nav .Select .Datetime").style.visibility="hidden"
  } 
  else {
    document.querySelector(".Nav .Select .Datetime").style.visibility="visible"
    document.querySelector(".Nav .logo img").src = `assests/Icons for cities/${Select_value}.svg`;
    time(city_inheritance.dateAndTime, 0);
    var requestOptions = {
      method: 'GET',
      redirect: 'follow'
    };
    try{
    let abc=fetch(`https://soliton.glitch.me?city=${Select_value}`, requestOptions)
      .then(response => response.json())
      .then(result1 => {Object.assign(city_detail_result,result1);return(result1.city_Date_Time_Name)})
      .then(city_value=>{
        var raw = JSON.stringify({
          "city_Date_Time_Name": `${city_value}`,
          "hours": 5
        });
        
        var requestOptions = {
          method: 'POST',
          headers: myHeaders,
          body: raw,
          redirect: 'follow'
        };
        
        fetch("https://soliton.glitch.me/hourly-forecast", requestOptions)
          .then(response => response.json())
          .then(result => {console.log(city_inheritance.nextFiveHrs=result.temperature);city_inheritance.hourly_change(flag)})
          .catch(error => console.log("Error", error));
      })
      .catch(error => console.log('Error'));
      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "application/json");
    }
    catch(err){
      console.log("Error", error)
    }
      
      // city_detail_result=await abc.json()
      //  city_detail_result.hours="5"
    city_inheritance.hourly_change(flag)
    city_inheritance.temp_change(flag)
  }
}

// function for finding city
function city(key, value) {
  let prop = value;
  let city = Daily[prop];
  return city;
}

//Temp  grid update
// function Temperaturechange(flag) {
//   let a=city_inheritance
//   let hourimg = function () {
//     let hour = document.querySelector(".Hourly").children;
//     if (flag==1) {
//       for (index = 0; index < 11; index += 2){
//         hour[index].children[3].innerText = "Nil";
//         (hour[index].children[2]).children[0].src ="assests/General Images & Icons/warning.svg" 
//       }
//       return;
//     }
//     // ---------------Hourly img and data----------------
//     let hourtem = [0];
//     hourtem[0] = a.temperature;
//     hourtem.push(...a.nextFiveHrs);
//     for (let s in hourtem) {
//       hourtem[s] = hourtem[s].replace("°C", "");
//     }
//     let j = 0;
//     for (let index = 0; index < 11; index += 2) {
//       let image = hour[index].children[2];
//       let weather =
//         hourtem[j] < 18
//           ? "rainyIconBlack"
//           : hourtem[j] >= 18 && hourtem[j] <= 22
//           ? "windyIcon"
//           : hourtem[j] >= 23 && hourtem[j] <= 29
//           ? "cloudyIcon"
//           : "sunnyIconBlack";
//       image.querySelector("img").src = `assests/Weather Icons/${weather}.svg`;
//       if (hourtem[j] != undefined)
//         hour[index].children[3].innerText = hourtem[j] + "°C";
//       else {
//         hour[index].children[3].innerText =
//           Math.floor(Math.random() * (40 - 30)) + 30 + "°C";
//       }
//       j++;
//     }
//     // ----------------------------------------------------
//   }; //Call for hour image
//   setTimeout(hourimg(), 100);
//   // ---------------Temp grid data-----------------------
//   let city_key = Object.keys(a);
//   let i = 3;
//   if (flag == 0) {
//     for (let index = 0; index < tempchild.length; index++) {
//       if (index == 2) {
//         tempchild[index].children[1].innerHTML =
//           (a[city_key[3]].replace("°C", "") * 1.8 + 32).toFixed(0) + " F";
//       } else {
//         tempchild[index].children[1].innerHTML = a[city_key[i++]];
//       }
//     }
//   } else {
//     for (let index = 0; index < tempchild.length; index++) {
//       tempchild[index].children[1].innerHTML = "NIL";
//     }
//     alert("Invalid City");
//   }
// }
//==================================================================================================================

function width_button_visible(){
let width=((document.querySelector(".SFrame .slide")).clientWidth);
if(width>=(display.value*292)){document.querySelector(".SFrame button").style.visibility="hidden"
document.querySelector(".SFrame button.bbtn").style.visibility="hidden"
document.querySelector(".SFrame .slide").style.justifyContent= "center";
}
else{document.querySelector(".SFrame button").style.visibility="visible"
document.querySelector(".SFrame button.bbtn").style.visibility="visible"
document.querySelector(".SFrame .slide").style.justifyContent= "flex-start";}
};
// =================================================IIFE Function=====================================================
//loading function
(function () {
  sun.click();
  clearInterval(CI_box_time);
  CI_box_time=setInterval(function(){box_time(".SFrame .slide")}, 60000);
  document.getElementById("sel").value = "Kolkata";
  setTimeout(select_city, 100);
  document.querySelector(".Frame .FFrame .icon #sun").style.borderBottom ="2px solid var(--blue)";
  continents_display();
})();
//Select option values
(function () {
  data_key.sort();
  document.querySelector(".Nav .Select datalist").innerHTML = "";
  for (loop_index = 0; loop_index < data_key.length; loop_index++) {
    document.querySelector(".Nav .Select datalist").innerHTML += `<option value="${data_key[loop_index]}">`;
  }
  
})();
// ====================================================================================================================
//city card time
function box_time(b_time) {
  box = document.querySelector(b_time).children;
  for (index = 0; index < box.length; index++) {
    let value = box[index].querySelector(".time").innerText;
    let city_dummy = value.split(" ");
    let hour_dummy = city_dummy[0].split(":");
    city_h = hour_dummy[0];
    city_m = hour_dummy[1];
    city_am_pm = city_dummy[1];
    city_m++;
    if (city_m == 60) {
      city_h++;
      city_m = 0;
    }
    if (city_h == 13) {
      city_m = 0;
      city_h = 1;
    }
    if(city_h>11){if(city_am_pm=="AM")city_am_pm="PM"}
    box[index].querySelector(".time").innerText =
      ("0" + city_h).slice(-2) +
      ":" +
      ("0" + city_m).slice(-2) +
      " " +
      city_am_pm;
  }
}
//Continent Card display
function continents_display(){
  let sbox=document.querySelector(".GFrame .continental");
  sbox.innerHTML="";
  for(loop_index=0;loop_index<12;loop_index++){
    let sbox_value=eval(initial_sort_array[loop_index])
    let continent_name=sbox_value.timeZone.split("/")
    let continent_time=sbox_value.dateAndTime.split(",")
    let dummy_h=continent_time[1].split(" ")
    let dummy_time=dummy_h[1].split(":")
    dummy_time[0]=(dummy_time[0]<10)?"0"+dummy_time[0]:dummy_time[0]
    dummy_time[1]=(dummy_time[1]<10)?"0"+dummy_time[1]:dummy_time[1]
    dummy_time[1]=dummy_time[1].slice(-2)
    let display_time=dummy_time[0]+":"+dummy_time[1]+" "+dummy_h[2]
    sbox.innerHTML+=`<div class="sbox">
    <div class="row1">
      <div class="conti" id="col-1">${continent_name[0]}</div>
      <div class="temp" id="col-2">${sbox_value.temperature}</div>
    </div>
    <div class="row2">
      <div class="place" id="col-1">${sbox_value.cityName}, <span class="time">${display_time}</span></div>
      <div class="prediction" id="col-2">
        <img
          src="assests/Weather Icons/humidityIcon.svg"
          alt="humidity"
        />${sbox_value.humidity}
      </div>
    </div>
  </div>`
  }
  clearInterval(conti_time_function)
  conti_time_function=setInterval(function(){box_time(".GFrame .continental")},60000)
}
function Sort_function(){
if(Sort_function_conti==0){
  continent_sort_img.style.borderBottom="0px"
  if(Sort_function_temp==1){
    temperature_only_sort()
    temperature_sort_img.style.borderBottom="4px solid var(--blue)"
  }
  else{
    city_wise_sort()
    temperature_sort_img.style.borderBottom="0px"
  }
}
else{
  continent_sort_img.style.borderBottom="4px solid var(--blue)"
  if(Sort_function_temp==1){
    continent_sort()
    temperature_sort()
    temperature_sort_img.style.borderBottom="4px solid var(--blue)"
  }
  else{
    continent_only_sort()
    temperature_sort_img.style.borderBottom="0px"
  }
}
}
function continent_only_sort(){
  initial_sort_array=[]
  initial_sort_array.push(...city_array.sort((a, b) => {
      let fn=(a.timeZone).split("/")
      let sn=(b.timeZone).split("/")
      let fc=a.cityName
      let sc=a.cityName
      
        if(fn>sn){
          return 1
        }
        if(fn<sn){
          return -1
        }
        if(fn==sn){
          if(fc>sc){
            return 1
          }
          if(fc<sc){
            return -1
          }
        return 0;
        }
      }))
      continents_display()
    }
function continent_sort(){
  initial_sort_array=[]
  initial_sort_array.push(...city_array.sort((a, b) => {
      let fn=(a.timeZone).split("/")
      let sn=(b.timeZone).split("/")
      let fc=a.cityName
      let sc=a.cityName
      //if(conti_sort_flag==1){
        if(fn>sn){
          return 1
        }
        if(fn<sn){
          return -1
        }
        
        return 0;
        }
      // }
      // else{
      //   if(fn<sn){
      //     return 1
      //   }
      //   if(fn>sn){
      //     return -1
      //   }
      // return 0;
      // }
    ))
    // if(conti_sort_flag==-1){document.querySelector(".GFrame .heading .cn img").src="assests/General Images & Icons/arrowUp.svg"}
    // else{document.querySelector(".GFrame .heading .cn img").src="assests/General Images & Icons/arrowDown.svg"}
    // conti_sort_flag=(conti_sort_flag==1)?-1:1
}
function city_wise_sort(){
  initial_sort_array=[]
    initial_sort_array.push(...city_array.sort((a, b) => {
      let fn=a.cityName
      let sn=b.cityName
      if(fn>sn){
        return 1
      }
      if(fn<sn){
        return -1
      }
    return 0;}))
    continents_display()
}
function temperature_only_sort(){
  initial_sort_array=[]
  initial_sort_array.push(...city_array.sort((a, b) => {
  let fnt=(a.temperature).replace("°C","")
  let snt=(b.temperature).replace("°C","")
  // if(fnt<10){fnt="0"+fnt}
  // if(snt<10){snt="0"+snt}
  if(+(fnt)>+(snt)){return 1}
  if(+(fnt)<+(snt)){return -1}
  return 0
}))
continents_display();
}
function temperature_sort(){
  initial_sort_array=[]
  initial_sort_array.push(...city_array.sort((a, b) => {
    let fn=(a.timeZone).split("/")
    let sn=(b.timeZone).split("/")
    let fnt=(a.temperature).replace("°C","")
    let snt=(b.temperature).replace("°C","")
    
    if(fn[0]==sn[0]){
      //if(temp_sort_flag==1){
      if(+(fnt)>+(snt)){return 1}
      if(+(fnt)<+(snt)){return -1}
      return 0
    // }
    //   else{
    //     if(fnt<snt){return 1}
    //   if(fnt>snt){return -1}
    //   return 0
    //   }
    }
  return 0;}))
  // if(temp_sort_flag==-1){document.querySelector(".GFrame .heading #temperature img").src="assests/General Images & Icons/arrowUp.svg"}
  //   else{document.querySelector(".GFrame .heading #temperature img").src="assests/General Images & Icons/arrowDown.svg"}
  //   temp_sort_flag=(temp_sort_flag==1)?-1:1
  continents_display()
}
//sorting functions
function temperature_sunny(value) {
  return (
    value.temperature.replace("°C", "") >= 29 ||
    value.humidity.replace("%", "") <= 50
  );
}
function temperature_cold(value) {
  return (
    (value.temperature.replace("°C", "") > 20 &&
      value.temperature.replace("°C", "") < 29 &&
      value.precipitation.replace("%", "") > 50) ||
    value.humidity.replace("%", "") > 50
  );
}
function temperature_rainy(value) {
  return (
    value.temperature.replace("°C", "") < 20 ||
    value.humidity.replace("%", "") >= 50
  );
}}